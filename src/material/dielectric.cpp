#include <cmath>

#include "dielectric.h"
#include "../hittable/hit_record.h"
#include "../utils/math.h"

double dielectric::schlick_reflectance(double costheta, double etar) {
    double ref0 = (1 - etar)/(1 + etar);
    ref0 *= ref0;
    return ref0 + (1 - ref0)*pow(1 - costheta, 5);
}

dielectric::dielectric(double eta) : material(color(1.0, 1.0, 1.0)) {
    _eta = eta;
}

bool dielectric::scatter(const ray &r, const hit_record &hr, color &attenuation, ray &scaterred) const {
    double etar = hr.front ? 1/_eta : _eta;
    double costheta = fmin(dot(-r.v, hr.n), 1.0);
    double sintheta = sqrt(1.0 - costheta*costheta);
    vec3 v;
    if (etar*sintheta > 1.0 || schlick_reflectance(costheta, etar) > randomd())
        v = reflect(r.v, hr.n);
    else
        v = refract(r.v, hr.n, etar);
    scaterred.set_v(v);
    scaterred.set_ray0(hr.p);
    attenuation = _albedo;
    return true;
}
