#ifndef METAL_H
#define METAL_H

#include "material.h"

class metal : public material {
private:
    double _fuzz;
public:
    metal(const color &, double);

    bool scatter(const ray&, const hit_record &, color &, ray &) const override;
};

#endif // METAL_H
