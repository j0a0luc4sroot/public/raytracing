#ifndef LAMBERTIAN_H
#define LAMBERTIAN_H

#include "material.h"

class lambertian : public material {
public:
    using material::material;

    bool scatter(const ray&, const hit_record &, color &, ray &) const override;
};

#endif // LAMBERTIAN_H
