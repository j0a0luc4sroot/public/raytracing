#ifndef MATERIAL_H
#define MATERIAL_H

#include "../ds/color.h"
#include "../ds/ray.h"

class hit_record;

class material {
protected:
    color _albedo;
public:
    material(const color &);
    virtual bool scatter(const ray&, const hit_record &, color &, ray &) const;
};

#endif // MATERIAL_H
