#ifndef DIELECTRIC_H
#define DIELECTRIC_H

#include "material.h"

class dielectric : public material {
private:
    double _eta;

    static double schlick_reflectance(double, double);
public:
    dielectric(double);

    bool scatter(const ray&, const hit_record &, color &, ray &) const override;
};

#endif // DIELECTRIC_H
