#include "metal.h"
#include "../hittable/hit_record.h"

metal::metal(const color &albedo, double fuzz) : material(albedo) {
    _fuzz = fuzz;
}

bool metal::scatter(const ray &r, const hit_record &hr, color &attenuation, ray &scaterred) const {
    vec3 v = (reflect(r.v, hr.n).unit() + _fuzz*vec3::random_unit_sphere()).unit();
    scaterred.set_v(v.unit());
    scaterred.set_ray0(hr.p);
    attenuation = _albedo;
    return dot(v, hr.n) > 0;

}
