#include "lambertian.h"
#include "../hittable/hit_record.h"

bool lambertian::scatter(const ray &r, const hit_record &hr, color &attenuation, ray &scaterred) const {
    vec3 v = diffuse_reflect(r.v, hr.n);
    scaterred.set_v(v);
    scaterred.set_ray0(hr.p);
    attenuation = _albedo;
    return true;
}
