#include <algorithm>
#include <cmath>
#include <iostream>

#include "camera.h"

void camera::initialize() {
    _image_height = int (image_width / aspect_ratio);
    _image_height = std::max(_image_height, 1);

    _camera_position = lookfrom;

    double h = tan(deg_to_rad(vfov)/2);
    double viewport_height = 2*h*focus_dist;
    double viewport_width = viewport_height * ((double) image_width / _image_height);

    _k = (lookfrom - lookat).unit();
    _i = cross(vup, _k).unit();
    _j = cross(_k, _i);

    vec3 viewport_i = viewport_width*_i;
    vec3 viewport_j = -viewport_height*_j;

    _dpixel_i = viewport_i / image_width;
    _dpixel_j = viewport_j / _image_height;

    point3 viewport_upper_left = _camera_position
        - focus_dist*_k - 0.5*(viewport_i + viewport_j);
    _pixel_upper_left = viewport_upper_left + 0.5*(_dpixel_i + _dpixel_j);

    _pixels_per_sample = 1.0 / num_samples;

    double defocus_radius = focus_dist * tan(deg_to_rad(defocus_angle/2));
    _ddefocus_disk_i = defocus_radius*_i;
    _ddefocus_disk_j = defocus_radius*_j;
}

ray camera::get_ray(int i, int j) const {
    vec3 offset = vec3(randomd() - 0.5, randomd() - 0.5, 0);
    vec3 rv = vec3::random_disk();
    point3 pixel = _pixel_upper_left +
        ((i + offset.x)*_dpixel_i) +
        ((j + offset.y)*_dpixel_j);
    point3 ray0 = _camera_position + rv.x*_ddefocus_disk_i + rv.y*_ddefocus_disk_j;

    return ray(ray0, (pixel - ray0).unit());
}

color camera::ray_color(const ray& r, const hittable& world, const int depth = 0) const {
    if (depth == max_depth)
        return color();

    hit_record hr;

    if (world.hit(r, interval(+infinitesimal, +inf), hr)) {
        ray scattered;
        color attenuation;
        if (hr.m->scatter(r, hr, attenuation, scattered))
            return attenuation * ray_color(scattered, world, depth + 1);
        return color();
    }

    vec3 unit_v = r.v.unit();
    auto a = (unit_v.y + 1.0)/2.0;
    return (1.0-a)*color(1.0, 1.0, 1.0) + a*color(0.5, 0.7, 1.0);
}



camera::camera() {
    image_width = 1920;
    aspect_ratio = 16.0 / 9.0;
    num_samples = 10;
    max_depth = 10;

    vfov = 90;
    lookfrom = point3(0, 0, 0);
    lookat = point3(0, 0, -1);
    vup = vec3(0, 1, 0);

    defocus_angle = 0;
    focus_dist = 10;
}



void camera::render(const hittable &world, std::ostream &out, std::ostream &log) {
    initialize();

    out << "P3\n";
    out << image_width << ' ' << _image_height << '\n';
    out << NUMCOLORS - 1 << "\n";

    for (int j = 0; j < _image_height; j++) {
        log << "Scanlines remaining: " << _image_height - j << '\n' << std::flush;
        for (int i = 0; i < image_width; i++) {
            color c;
            for (int sample = 0; sample < num_samples; sample++) {
                ray r = get_ray(i, j);
                c += ray_color(r, world);
            }
            out << c * _pixels_per_sample << '\n';
        }
    }
    log << "Done\n";
}
