#ifndef CAMERA_H
#define CAMERA_H

#include <iostream>
#include <ostream>

#include "../ds/color.h"
#include "../ds/interval.h"
#include "../ds/point3.h"
#include "../ds/ray.h"
#include "../ds/vec3.h"
#include "../hittable/hit_record.h"
#include "../hittable/hittable.h"
#include "../utils/math.h"

class camera {
private:
    int _image_height;
    point3 _camera_position;
    point3 _pixel_upper_left;
    vec3 _dpixel_i;
    vec3 _dpixel_j;
    double _pixels_per_sample;
    vec3 _i, _j, _k;
    vec3 _ddefocus_disk_i;
    vec3 _ddefocus_disk_j;

    void initialize();
    ray get_ray(int, int) const;
    color ray_color(const ray&, const hittable&, const int) const;

public:
    int image_width;
    double aspect_ratio;
    int num_samples;
    int max_depth;

    double vfov;
    point3 lookfrom;
    point3 lookat;
    vec3 vup;

    double defocus_angle;
    double focus_dist;

    camera();

    void render(const hittable &, std::ostream & = std::cout, std::ostream & = std::clog);
};

#endif // CAMERA_H
