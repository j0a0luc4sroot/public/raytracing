#include <utility>

#include "hit_record.h"

hit_record &hit_record::operator=(const hit_record &other) {
    _n = other._n;
    _front = other._front;
    p = other.p;
    t = other.t;
    m = other.m;
    return *this;
}

hit_record &hit_record::operator=(hit_record &&other) {
    _n = std::move(other._n);
    _front = std::move(other._front);
    p = std::move(other.p);
    t = std::move(other.t);
    m = std::move(other.m);
    return *this;
}



void hit_record::set_n(const ray &r, const vec3 &outward_n) {
    _front = dot(r.v, outward_n) < 0;
    _n = _front ? outward_n : -outward_n;
}
