#include "hittables.h"
#include <memory>

hittables::hittables() : hittable{nullptr} {
}



void hittables::push(std::shared_ptr<hittable> ph) {
    _hittables.push_back(ph);
}

void hittables::clear() {
    _hittables.clear();
}



bool hittables::hit(const ray &r, const interval &i, hit_record &hr) const {
    hit_record aux_hr;
    bool hit_anything = false;
    double closest_so_far = i.max;
    
    for (const std::shared_ptr<hittable> &ph : _hittables)
        if (ph->hit(r, interval(i.min, closest_so_far), aux_hr)) {
            hit_anything = true;
            closest_so_far = aux_hr.t;
            hr = aux_hr;
        }

    return hit_anything;
}
