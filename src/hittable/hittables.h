#ifndef HITTABLES_H
#define HITTABLES_H

#include <memory>
#include <vector>

#include "../ds/ray.h"
#include "../ds/interval.h"
#include "hittable.h"

class hittables : public hittable {
private:
    std::vector<std::shared_ptr<hittable>> _hittables;
public:
    hittables();

    void push(std::shared_ptr<hittable> ph);
    void clear();

    bool hit(const ray &, const interval &, hit_record &) const override;
};

#endif // HITTABLES_H
