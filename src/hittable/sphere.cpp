#include <cmath>

#include "sphere.h"

sphere::sphere(const point3 &center, double rd, std::shared_ptr<const material> m) : hittable{m} {
    _center = center;
    _rd = rd;
}

bool sphere::hit(const ray &r, const interval &i, hit_record &hr) const {
    vec3 ray0center = _center - r.ray0;
    double a = r.v.energy();
    double h = dot(r.v, ray0center);
    double c = ray0center.energy() - _rd*_rd;
    double delta = h*h - a*c;
    if (delta < 0)
        return false;

    double sqrtdelta = sqrt(delta);

    double t = (h - sqrtdelta)/a;
    if (!i.surrounds(t)) {
        t = (h + sqrtdelta)/a;
        if (!i.surrounds(t))
            return false;
    }

    hr.p = r.at(t);
    hr.set_n(r, (hr.p - _center)/_rd);
    hr.t = t;
    hr.m = _m;

    return true;
}
