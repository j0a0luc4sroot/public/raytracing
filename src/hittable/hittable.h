#ifndef HITTABLE_H
#define HITTABLE_H

#include <memory>

#include "../ds/ray.h"
#include "../ds/interval.h"
#include "hit_record.h"

class material;

class hittable {
protected:
    std::shared_ptr<const material> _m;
public:
    hittable(std::shared_ptr<const material>);
    virtual bool hit(const ray &, const interval &, hit_record &) const = 0;
};

#endif // HITTABLE_H
