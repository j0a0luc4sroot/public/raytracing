#ifndef SPHERE_H
#define SPHERE_H

#include <memory>

#include "../ds/point3.h"
#include "../ds/interval.h"
#include "hittable.h"

class sphere : public hittable {
private:
    point3 _center;
    double _rd;
public:
    sphere(const point3 &, double, std::shared_ptr<const material>);
    bool hit(const ray &, const interval &, hit_record &) const override;
};

#endif // SPHERE_H
