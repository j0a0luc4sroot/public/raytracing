#ifndef HIT_RECORD_H
#define HIT_RECORD_H

#include <memory>

#include "../ds/point3.h"
#include "../ds/ray.h"
#include "../ds/vec3.h"
#include "../material/material.h"

class hit_record {
private:
    vec3 _n;
    bool _front;
public:
    const vec3 &n = _n;
    const bool &front = _front;

    point3 p;
    double t;
    std::shared_ptr<const material> m;

    hit_record &operator=(const hit_record &);
    hit_record &operator=(hit_record &&);

    void set_n(const ray &, const vec3 &);
};

#endif // HIT_RECORD_H
