#include <chrono>
#include <memory>

#include "camera/camera.h"
#include "ds/point3.h"
#include "hittable/hittables.h"
#include "hittable/sphere.h"
#include "material/dielectric.h"
#include "material/lambertian.h"
#include "material/metal.h"
#include "math.h"

#define IMAGE_WIDTH 400
#define ASPECT_RATIO (16.0 / 9.0)
#define NUM_SAMPLES 10
#define MAX_DEPTH 10

#define VFOV 20.0
#define LOOKFROM 13.0, 2.0, 3.0
#define LOOKAT 0.0, 0.0, 0.0
#define VUP 0.0, 1.0, 0.0

#define DEFOCUS_ANGLE 0.6
#define FOCUS_DIST 10.0

int main() {
    
    hittables world;

    auto ground_material = std::make_shared<lambertian>(color(0.5, 0.5, 0.5));
    world.push(std::make_shared<sphere>(point3(0,-1000,0), 1000, ground_material));

    for (int a = -11; a < 11; a++) {
        for (int b = -11; b < 11; b++) {
            double choose_mat = randomd();
            point3 center(a + 0.9*randomd(), 0.2, b + 0.9*randomd());

            if ((center - point3(4, 0.2, 0)).norm() > 0.9) {
                std::shared_ptr<material> sphere_material;

                if (choose_mat < 0.8) {
                    // diffuse
                    color albedo = color(randomd()*randomd(),
                                         randomd()*randomd(),
                                         randomd()*randomd());
                    sphere_material = std::make_shared<lambertian>(albedo);
                    world.push(std::make_shared<sphere>(center, 0.2, sphere_material));
                } else if (choose_mat < 0.95) {
                    // metal
                    color albedo = color(randomd(0.5, 1)*randomd(0.5, 1),
                                         randomd(0.5, 1)*randomd(0.5, 1),
                                         randomd(0.5, 1)*randomd(0.5, 1));
                    double fuzz = randomd(0, 0.5);
                    sphere_material = std::make_shared<metal>(albedo, fuzz);
                    world.push(std::make_shared<sphere>(center, 0.2, sphere_material));
                } else {
                    // glass
                    sphere_material = std::make_shared<dielectric>(1.5);
                    world.push(std::make_shared<sphere>(center, 0.2, sphere_material));
                }
            }
        }
    }

    auto material1 = std::make_shared<dielectric>(1.5);
    world.push(std::make_shared<sphere>(point3(0, 1, 0), 1.0, material1));

    auto material2 = std::make_shared<lambertian>(color(0.4, 0.2, 0.1));
    world.push(std::make_shared<sphere>(point3(-4, 1, 0), 1.0, material2));

    auto material3 = std::make_shared<metal>(color(0.7, 0.6, 0.5), 0.0);
    world.push(std::make_shared<sphere>(point3(4, 1, 0), 1.0, material3));

    camera cam;

    cam.image_width = IMAGE_WIDTH;
    cam.aspect_ratio = ASPECT_RATIO;
    cam.num_samples = NUM_SAMPLES;
    cam.max_depth = MAX_DEPTH;

    cam.vfov = VFOV;
    cam.lookfrom = point3(LOOKFROM);
    cam.lookat = point3(LOOKAT);
    cam.vup = vec3(VUP);

    cam.defocus_angle = DEFOCUS_ANGLE;
    cam.focus_dist    = FOCUS_DIST;

    auto start = std::chrono::steady_clock::now();
    cam.render(world);
    auto end = std::chrono::steady_clock::now();
    std::clog << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()/1000.0 << "s\n";
}
