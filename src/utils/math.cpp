#include "math.h"

#include <algorithm>
#include <cmath>
#include <cstdlib>

double deg_to_rad(double deg) {
    return deg * pi / 180.0;
}

double rad_to_deg(double rad) {
    return rad * 180.0 / pi;
}



double randomd() {
    return rand() / (RAND_MAX + 1.0);
}

double randomd(double min, double max) {
    return min + (max - min) * randomd();
}



double linear_to_gamma(double linear) {
    return sqrt(std::max(linear, 0.0));
}
