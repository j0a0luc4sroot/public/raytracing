#ifndef UTILS_H
#define UTILS_H

#include <limits>

static const double infinitesimal = 0.001;
static const double inf = std::numeric_limits<double>::infinity();
static const double pi = 3.1415926535897932385;

double deg_to_rad(double);
double rad_to_deg(double);

double randomd();
double randomd(double, double);

double linear_to_gamma(double);

#endif // UTILS_H
