#include <utility>
#include <cmath>

#include "vec3.h"
#include "../utils/math.h"

void vec3::set_x(double x) {
    _x = x;
}

void vec3::set_y(double y) {
    _y = y;
}

void vec3::set_z(double z) {
    _z = z;
}

vec3::vec3() {
    _x = 0;
    _y = 0;
    _z = 0;
}

vec3::vec3(double x, double y, double z) {
    _x = x;
    _y = y;
    _z = z;
}



vec3 &vec3::operator=(const vec3 &other) {
    _x = other.x;
    _y = other.y;
    _z = other.z;
    return *this;
}

vec3 &vec3::operator=(vec3 &&other) {
    _x = std::move(other.x);
    _y = std::move(other.y);
    _z = std::move(other.z);
    return *this;
}



vec3 &vec3::operator+=(const vec3 &other) {
    _x += other._x;
    _y += other._y;
    _z += other._z;
    return *this;
}

vec3 &vec3::operator-=(const vec3 &other) {
    _x -= other._x;
    _y -= other._y;
    _z -= other._z;
    return *this;
}



vec3 &vec3::operator*=(double d) {
    _x *= d;
    _y *= d;
    _z *= d;
    return *this;
}

vec3 &vec3::operator/=(double d) {
    _x /= d;
    _y /= d;
    _z /= d;
    return *this;
}



vec3 vec3::operator+(const vec3 &other) const {
    return vec3(_x + other._x, _y + other._y, _z + other._z);
}

vec3 vec3::operator-(const vec3 &other) const {
    return vec3(_x - other._x, _y - other._y, _z - other._z);
}



vec3 vec3::operator+() const {
    return vec3(+_x, +_y, +_z);
}

vec3 vec3::operator-() const {
    return vec3(-_x, -_y, -_z);
}



vec3 vec3::operator*(double d) const {
    return vec3(_x*d, _y*d, _z*d);
}

vec3 vec3::operator/(double d) const {
    return vec3(_x/d, _y/d, _z/d);
}



double vec3::energy() const {
    return _x*_x + _y*_y + _z*_z;
}

double vec3::norm() const {
    return sqrt(energy());
}

vec3 vec3::unit() const {
    return *this / norm();
}



bool vec3::near_zero() const {
    static const double infinitesimal = 1e-8;
    return std::fabs(_x) < infinitesimal &&
    std::fabs(_y) < infinitesimal &&
    std::fabs(_z) < infinitesimal;
}



vec3 vec3::random() {
    return vec3(randomd(), randomd(), randomd());
}

vec3 vec3::random(double min, double max) {
    return vec3(randomd(min, max), randomd(min, max), randomd(min, max));
}



vec3 vec3::random_unit_sphere() {
    while (true) {
        vec3 v = random(-1, 1);
        if (infinitesimal*infinitesimal < v.energy() && v.energy() < 1)
            return v.unit();
    }
}

vec3 vec3::random_unit_hemisphere(const vec3 &n) {
    vec3 v = random_unit_sphere();
    return dot(v, n) > 0.0 ? v : -v;
}

vec3 vec3::random_disk() {
    while (true) {
        vec3 v = vec3(randomd(-1, 1), randomd(-1, 1), 0);
        if (infinitesimal*infinitesimal < v.energy() && v.energy() < 1)
            return v;
    }
}



vec3 operator*(double d, const vec3 &v) {
    return v*d;
}



double dot(const vec3 &u, const vec3 &v) {
    return u.x*v.x + u.y*v.y + u.z*v.z;
}

vec3 cross(const vec3 &u, const vec3 &v) {
    return vec3(
        u.y*v.z - u.z*v.y,
        u.z*v.x - u.x*v.z,
        u.x*v.y - u.y*v.x
    );
}



vec3 diffuse_reflect(const vec3 &in, const vec3 &n) {
    vec3 out = n + vec3::random_unit_sphere();
    if (out.near_zero())
        out = n;
    out = out.unit();
    return out;
}

vec3 reflect(const vec3 &in, const vec3 &n) {
    vec3 out = in - 2*dot(in, n)*n;
    return out;
}

vec3 refract(const vec3 &in, const vec3 &n, double etar) {
    double costheta = fmin(dot(-in, n), 1.0);
    vec3 outt = etar*(in + costheta*n);
    return outt - sqrt(fabs(1.0 - outt.energy()))*n;
}
