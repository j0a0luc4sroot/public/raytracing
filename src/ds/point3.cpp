#include <utility>

#include "point3.h"

void point3::set_x(double x) {
    _x = x;
}

void point3::set_y(double y) {
    _y = y;
}

void point3::set_z(double z) {
    _z = z;
}

point3::point3() {
    _x = 0;
    _y = 0;
    _z = 0;
}

point3::point3(double x, double y, double z) {
    _x = x;
    _y = y;
    _z = z;
}



point3 &point3::operator=(const point3 &other) {
    _x = other._x;
    _y = other._y;
    _z = other._z;
    return *this;
}

point3 &point3::operator=(point3 &&other) {
    _x = std::move(other._x);
    _y = std::move(other._y);
    _z = std::move(other._z);
    return *this;
}



point3 &point3::operator+=(const vec3 &v) {
    _x += v.x;
    _y += v.y;
    _z += v.z;
    return *this;
}

point3 &point3::operator-=(const vec3 &v) {
    _x -= v.x;
    _y -= v.y;
    _z -= v.z;
    return *this;
}



point3 point3::operator+(const vec3 &v) const {
    return point3(_x + v.x, _y + v.y, _z + v.z);
}

point3 point3::operator-(const vec3 &v) const {
    return point3(_x - v.x, _y - v.y, _z - v.z);
}



vec3 point3::operator-(const point3 &other) const {
    return vec3(_x - other._x, _y - other._y, _z - other._z);
}
