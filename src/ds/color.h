#ifndef COLOR_H
#define COLOR_H

#include <ostream>

#define NUMCOLORS (256)

class color {
private:
    double _r, _g, _b;
public:
    const double &r = _r;
    const double &g = _g;
    const double &b = _b;

    void set_r(double);
    void set_g(double);
    void set_b(double);

    color();
    color(double, double, double);

    color(const color &) = default;
    color(color &&) = default;

    color &operator=(const color &);
    color &operator=(color &&);

    color &operator+=(const color &);
    color &operator-=(const color &);

    color &operator*=(double);
    color &operator/=(double);

    color &operator*=(const color &);

    color operator+(const color &) const;
    color operator-(const color &) const;

    color operator+() const;
    color operator-() const;

    color operator*(double) const;
    color operator/(double) const;

    color operator*(const color &) const;
};

color operator*(double, const color &);

std::ostream &operator<<(std::ostream &, const color &);

#endif // COLOR_H
