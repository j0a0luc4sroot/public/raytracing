#include <cmath>

#include "ray.h"

void ray::set_ray0(const point3 &ray0) {
    _ray0 = ray0;
}

void ray::set_v(const vec3 &v) {
    _v = v;
}

ray::ray() {
}

ray::ray(const point3 &ray0, const vec3 &v) {
    _ray0 = ray0;
    _v = v;
}



point3 ray::at(double t) const {
    return _ray0 + _v*t;
}
