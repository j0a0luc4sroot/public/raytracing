#ifndef INTERVAL_H
#define INTERVAL_H

class interval {
private:
    double _min, _max;
public:
    const double &min = _min;
    const double &max = _max;

    interval();
    interval(double, double);

    void set_min(double);
    void set_max(double);

    double size() const;

    bool contains(double) const;
    bool surrounds(double) const;

    double clamp(double) const;

    static const interval empty, universe;
};

#endif // INTERVAL_H
