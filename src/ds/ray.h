#ifndef RAY_H
#define RAY_H

#include "color.h"
#include "point3.h"
#include "vec3.h"

class ray {
private:
    point3 _ray0;
    vec3 _v;
public:
    const point3 &ray0 = _ray0;
    const vec3 &v = _v;

    void set_ray0(const point3 &);
    void set_v(const vec3 &);

    ray();
    ray(const point3 &, const vec3 &);

    point3 at(double) const;
};

#endif // RAY_H
