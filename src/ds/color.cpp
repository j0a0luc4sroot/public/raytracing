#include <utility>

#include "color.h"
#include "interval.h"
#include "../utils/math.h"

void color::set_r(double r) {
    _r = r;
}

void color::set_g(double g) {
    _g = g;
}

void color::set_b(double b) {
    _b = b;
}

color::color() {
    _r = 0;
    _g = 0;
    _b = 0;
}

color::color(double r, double g, double b) {
    _r = r;
    _g = g;
    _b = b;
}



color &color::operator=(const color &other) {
    _r = other.r;
    _g = other.g;
    _b = other.b;
    return *this;
}

color &color::operator=(color &&other) {
    _r = std::move(other.r);
    _g = std::move(other.g);
    _b = std::move(other.b);
    return *this;
}



color &color::operator+=(const color &other) {
    _r += other._r;
    _g += other._g;
    _b += other._b;
    return *this;
}

color &color::operator-=(const color &other) {
    _r -= other._r;
    _g -= other._g;
    _b -= other._b;
    return *this;
}



color &color::operator*=(double d) {
    _r *= d;
    _g *= d;
    _b *= d;
    return *this;
}

color &color::operator/=(double d) {
    _r /= d;
    _g /= d;
    _b /= d;
    return *this;
}



color &color::operator*=(const color &other) {
    _r *= other._r;
    _g *= other._g;
    _b *= other._b;
    return *this;
}



color color::operator+(const color &other) const {
    return color(_r + other._r, _g + other._g, _b + other._b);
}

color color::operator-(const color &other) const {
    return color(_r - other._r, _g - other._g, _b - other._b);
}



color color::operator+() const {
    return color(+_r, +_g, +_b);
}

color color::operator-() const {
    return color(-_r, -_g, -_b);
}



color color::operator*(double d) const {
    return color(_r*d, _g*d, _b*d);
}

color color::operator/(double d) const {
    return color(_r/d, _g/d, _b/d);
}



color color::operator*(const color &other) const {
    return color(_r*other._r, _g*other._g, _b*other._b);
}



color operator*(double d, const color &c) {
    return c*d;
}



std::ostream &operator<<(std::ostream &out, const color &c) {
    static const interval i(0, 0.999);
    return out << (int) (NUMCOLORS*i.clamp(linear_to_gamma(c.r))) << ' '
               << (int) (NUMCOLORS*i.clamp(linear_to_gamma(c.g))) << ' '
               << (int) (NUMCOLORS*i.clamp(linear_to_gamma(c.b)));
}
