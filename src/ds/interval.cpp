#include "interval.h"
#include "../utils/math.h"

interval::interval() {
    _min = +inf;
    _max = -inf;
}

interval::interval(double min, double max) {
    _min = min;
    _max = max;
}



void interval::set_min(double min) {
    _min = min;
}

void interval::set_max(double max) {
    _max = max;
}



double interval::size() const {
    return _max - _min;
}



bool interval::contains(double value) const {
    return _min <= value && value <= _max;
}

bool interval::surrounds(double value) const {
    return _min < value && value < _max;
}



double interval::clamp(double value) const {
    if (value < _min) return _min;
    if (value > _max) return _max;
    return value;
}



const interval interval::empty = interval(+inf, -inf);

const interval interval::universe = interval(-inf, +inf);
