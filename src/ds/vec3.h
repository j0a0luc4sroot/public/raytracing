#ifndef VEC3_H
#define VEC3_H

class vec3 {
private:
    double _x, _y, _z;
public:
    const double &x = _x;
    const double &y = _y;
    const double &z = _z;

    void set_x(double);
    void set_y(double);
    void set_z(double);

    vec3();
    vec3(double, double, double);

    vec3(const vec3 &) = default;
    vec3(vec3 &&) = default;

    vec3 &operator=(const vec3 &);
    vec3 &operator=(vec3 &&);

    vec3 &operator+=(const vec3 &);
    vec3 &operator-=(const vec3 &);

    vec3 &operator*=(double);
    vec3 &operator/=(double);

    vec3 operator+(const vec3 &) const;
    vec3 operator-(const vec3 &) const;

    vec3 operator+() const;
    vec3 operator-() const;

    vec3 operator*(double) const;
    vec3 operator/(double) const;

    double energy() const;
    double norm() const;
    vec3 unit() const;

    bool near_zero() const;

    static vec3 random();
    static vec3 random(double, double);

    static vec3 random_unit_sphere();
    static vec3 random_unit_hemisphere(const vec3 &);
    static vec3 random_disk();
};

vec3 operator*(double, const vec3 &);

double dot(const vec3 &, const vec3 &);
vec3 cross(const vec3 &, const vec3 &);

vec3 diffuse_reflect(const vec3 &, const vec3 &);
vec3 reflect(const vec3 &, const vec3 &);
vec3 refract(const vec3 &, const vec3 &, double);

#endif // VEC3_H
