#ifndef POINT3_H
#define POINT3_H

#include "vec3.h"

class point3 {
private:
    double _x, _y, _z;
public:
    const double &x = _x;
    const double &y = _y;
    const double &z = _z;

    void set_x(double);
    void set_y(double);
    void set_z(double);

    point3();
    point3(double, double, double);

    point3(const point3 &) = default;
    point3(point3 &&) = default;

    point3 &operator=(const point3 &);
    point3 &operator=(point3 &&);

    point3 &operator+=(const vec3 &);
    point3 &operator-=(const vec3 &);

    point3 operator+(const vec3 &) const;
    point3 operator-(const vec3 &) const;

    vec3 operator-(const point3 &) const;
};

#endif // POINT3_H
