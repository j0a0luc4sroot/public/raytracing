# Ray Tracing

![raytracing](resources/raytracing.png)

Ray Tracing implementation in C++ based on Ray Tracing in One Weekend book series.

## Building

```
make
```

## Running

```
make run
```

## References
* [_Ray Tracing in One Weekend_](https://raytracing.github.io/books/RayTracingInOneWeekend.html)
